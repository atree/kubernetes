![foldingathome](img/foldingathome.png)

### Docker Images and Official Documentation
* [FoldingAtHome arm64 Docker Image](https://hub.docker.com/r/beastob/foldingathome-arm64)
* [FoldingAtHome Documentation](https://test.foldingathome.org/support/faq/installation-guides/configuration-guide/?lng=en-US)

### Yaml Files
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 00-secrets.yaml
* 10-persistentVolumes.yaml
* 30-configMaps.yaml
* 55-statefulsets.yaml

### Deployment Instructions
For now this deployment doesn't have any dependencies, so just deploy foldingathome.

* `kubectl apply -f foldingathome/`

### Minikube notes

### Configuration
The configuration is done mostly with environment variables in the deployments file.  The values are stored in secrets.  I'm using persistent volues for storage on a glusterfs cluster which ensures that wwhichever node the pod comes up on, it can access it's data and that data will survive a pod relaunch.  The telegraf sidecar groks the foldingathome log and forwards the percent done to the influxdb database so progress can be graphed.

### TODO
