![](files/kubernetes.png)

### Links to Images and Documentation
* [Docker Images](https://hub.docker.com/)
* [Kubernetes Documentation](https://kubernetes.io/docs/home/)

### Links to specific k8s resources
* [Kubernetes Namespaces](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)
* [Kubernetes Services](https://kubernetes.io/docs/concepts/services-networking/service/)
* [Kubernetes Deployments](https://kubernetes.io/docs/concepts/services-networking/service/)
* [Kubernetes Stateful Sets](https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/)
* [Kubernetes Secrets](https://kubernetes.io/docs/concepts/configuration/secret/)

### Notes
So far all of my work is being built and tested in either minikube or my Raspberry Pi 4B 8GB cluster.

All shared passwords are configured in the secrets directory, and deployment specific secrets are stored in a 00- file in the deployment directory. The secrets directory as well as 00- files are transcripted

### TODO:

