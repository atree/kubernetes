#!/bin/bash
if [[ $# == 0 ]]; then
  echo "Usage: $0 [host] [index name] [# shards] [# replicas]"
  echo "   or: $0 [host] [index name] (to delete)"
else
  if [[ $# == 4 ]]; then
    host=$1
    name=$2
    shards=$3
    replicas=$4

    curl -X PUT "${host}:9200/${name}?pretty=true" -H 'Content-Type: application/json' -d "{ \"settings\": { \"number_of_shards\": $shards,  \"number_of_replicas\": $replicas } }"
  elif [[ $# == 2 ]]; then
    host=$1
    name=$2

    curl -X DELETE "${host}:9200/${name}?pretty=true"
  fi
fi
