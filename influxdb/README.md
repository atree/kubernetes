![influxdb](img/influxdb.png)

### Links to Images and Documentation
* [InfluxDB Docker Image](https://hub.docker.com/_/influxdb)
* [InfluxDB Documentation](https://docs.influxdata.com/influxdb/)

### Files 
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 10-persistentVolumes.yaml
* 20-persistentVolumeClaims.yaml
* 30-configMaps.yaml
* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
The passwords for influxdb are stored in secrets.  Deploy secrets first.

* `kubectl apply -f secrets/`
* `kubectl apply -f influxdb/`

### Minikube notes
* `minikube service influxdb --url [-n namespace]`

### Configuration
The /var/lib/influxdb directory is mounted from a persistent volume.  This has been writen and tested on minikube, I don't know if it will work exactly the same when I move it to the real cluster.  I used a persistent volume so that if the pod gets deleted and replaced the data in the database will survive that event.

The configMap creates the configuration for telegraf to send metrics to influxdb.  The config contains passwords so it references environment variables instead of passwords.  Those variables are set in the deployment file, pulling the passwords from secrets.

The service file creates a NodePort service since there is only 1 mysql server.  I'd love to have a cluster but InfluxDB is only free for a single node.


### TODO
* See if I can have multiple pods share the same data volume to make a more fault tolerant data layer.
