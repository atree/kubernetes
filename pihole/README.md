![pihole](img/pihole.png)

### Links to Images and Documentation
* [PiHole Docker Image](https://hub.docker.com/r/pihole/pihole)
* [Consul Docker Image](https://hub.docker.com/_/consul)
* [PiHole Documentation](https://docs.pi-hole.net/)

### Files 
Files are processed in order they are numbered.

* 00-secrets.yaml
* 10-persistentVolumes.yaml
* 20-persistentVolumeClaims.yaml
* 30-configMaps.yaml
* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
This deploys a consul sidecar that attempts to join the cluster, so Consul should be installed first
* `kubectl apply -f consul/`
* `kubectl apply -f pihole/`

### Minikube notes
* `minikube service pihole --url`

### Configuration
This deploys pihole to kubernetes with consul agent sidecars.  It's more of a way to play with pihole and consul/sidecars than to actually run as a production implimentation.  Unless I can figure out a way to get the actual client names to show in the logs instead of the gateway of the k8s cluster, it's not really useful.  It would work fine for strictly ad blocking, but not for permit/deny by client.

### TODO
Run gravity-sync as a sidecar to keep the databases in sync, or figure out another way to sync the dbs
