![elasticsearch](img/elasticsearch.png)

### Links to Images and Documentation
* [Docker Image](https://hub.docker.com/_/elasticsearch)
* [Elasticsearch Documentation](https://www.elastic.co/guide/en/* reference/current/index.html)

### Files
Files are processed in order they are numbered.

* 10-persistentVolumes.yaml
* 30-configMaps.yaml
* 40-services.yaml
* 55-statefulsets.yaml

### Deployment Instructions
* `kubectl apply -f elasticsearch/`

### Minikube notes
* `minikube service elasticsearch --url -n database`

### Configuration

### TODO
