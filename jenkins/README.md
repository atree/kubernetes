![jenkins](img/jenkins.png)

### Docker Images and Official Documentation
* [template Docker Image]()
* [template Documentation]()

### Yaml Files 
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

10-persistentVolumes.yaml
20-persistentVolumeClaims.yaml
40-services.yaml
50-deployments.yaml
55-deployments.yaml

### Deployment Instructions

* `kubectl apply -f <template/`

### Minikube notes
* `minikube service <template> --url [-n namespace]`

### Configuration

### TODO
