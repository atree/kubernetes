![vault](img/vault.png)

### Docker Images and Official Documentation
* [Vault Docker Image](https://hub.docker.com/_/vault)
* [Vault Documentation](https://www.vaultproject.io/docs)

### Yaml Files 
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 30-configMaps.yaml
* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
Vault uses consul as it's backend database for fault tollerance and redundancy.  So we need to deploy consul first.

* `kubectl apply -f consul/`
* `kubectl apply -f vault/`

### Minikube notes
* `minikube service vault [-n namespace]`

### Configuration
The configMap creates the configuration for vault.  

The service file creates 2 services, 1 for the consul webgui to be load balanced, and 1 for the internal ports.

### TODO
* This deployment is still in flux, not working right, lots of stuff wrong
* Figure out why the vault pods try to listen on consul ip addresses
