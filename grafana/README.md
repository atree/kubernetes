![grafana](img/grafana.png)

### Docker Images and Official Documentation
* [Grafana Docker Image](https://hub.docker.com/r/grafana/grafana)
* [Grafana Documentation](https://grafana.com/docs/)

### Yaml Files 
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
I configured this grafana deployment to store it's configuration in MySQL, therefore, we deploy MySQL before Grafana.  InfluxDB is the datasource for Grafana, as well as the endpoint for metrics in the MySQL deploy, so go ahead and deploy influxdb before mysql.  There are passwords for everything in secrets/, so best to install it before anything.

* `kubectl apply -f secrets/`
* `kubectl apply -f influxdb/`
* `kubectl apply -f mysql/`
* `kubectl apply -f grafana/`

### Minikube notes
Launch the grafana web ui in your default browser:

* `minikube service grafana`

### Configuration
Grafana configuration is stored in a MySQL Database.  Environment variables in the deployments file point to the database where the grafana configuration resides.  Users, Datasources, etc. are all stored there to prevent having to recreate this every time.  If this is a brand new deployment with no database, grafana will create the database.

The services yaml deploys a LoadBalancer service and will balance the load between the 2 grafana pods.

### TODO
