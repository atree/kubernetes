![kafka](img/kafka.png)

### Docker Images and Official Documentation
* [Kafka Docker Image](https://hub.docker.com/r/confluentinc/cp-kafka/)
* [Kafka Documentation](http://kafka.apache.org/documentation/)

### Yaml Files
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 40-services.yaml
* 55-statefulsets.yaml

### Deployment Instructions

* `kubectl apply -f zookeeper/`
* `kubectl apply -f kafka/`

### Minikube notes
* `kubectl get services` (to see which port forwards to 2181, or 8080, etc.)
* `minikube service zookeeper --url [-n namespace]`

### Configuration


### TODO
* Document this better! lol
