![mysql](img/mysql.png)

### Docker Images and Official Documentation
* [MySQL Docker Image](https://hub.docker.com/_/mysql)
* [MySQL Documentation](https://dev.mysql.com/doc/refman/5.7/en/)

### Yaml Files 
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 10-persistentVolumes.yaml
* 20-persistentVolumeClaims.yaml
* 30-configMaps.yaml
* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
This deployment includes a telegraf sidecar.  It will send metrics to InfluxDB, so influxdb should be installed before MySQL.

* `kubectl apply -f influxdb/`
* `kubectl apply -f mysql/`

### Minikube notes
* `minikube service mysql --url [-n namespace]`

### Configuration
The /var/lib/mysql directory is mounted from a persistent volume.  This has been writen and tested on minikube, I don't know if it will work exactly the same when I move it to the real cluster.  I used a persistent volume so that if the pod gets deleted and replaced the data in the database will survive that event.

The configMap creates the configuration for telegraf to send metrics to influxdb.  The config contains passwords so it references environment variables instead of passwords.  Those variables are set in the deployment file, pulling the passwords from secrets.

The service file creates a NodePort service since there is only 1 mysql server.  For now...

### TODO
* Look into having a more fault tollerant database layer.  Perhaps a multimaster MariaDB instead of MySQL
* Figure out how to deploy more databases than just the grafana one via environment variables
