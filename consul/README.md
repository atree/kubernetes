![consul](img/consul.png)

### Docker Images and Official Documentation
* [Consul Docker Image](https://hub.docker.com/_/consul)
* [Consul Documentation](https://www.consul.io/docs)

### Yaml Files
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 00-secrets.yaml
* 30-configMaps.yaml
* 40-services.yaml
* 50-deployments.yaml

### Deployment Instructions
For now this deployment doesn't have any dependencies, so just deploy consul.

* `kubectl apply -f consul/`

### Minikube notes
* `minikube service consul-ui [-n namespace]`

### Configuration
The configMap creates the configuration for consul.  There aren't any passwords in the config file yet, so there's no secrets references.

The service file creates 2 services, 1 for the consul web-ui to be load balanced, and 1 for the internal ports.

### TODO
* Generate a new encryption string and put it in secrets
