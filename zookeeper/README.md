![zookeeper](img/zookeeper.png)

### Docker Images and Official Documentation
* [Zookeeper Docker Image](https://hub.docker.com/_/zookeeper)
* [Zookeeper Documentation](https://zookeeper.apache.org/)

### Yaml Files
Files are processed in order they are numbered.  These are not official files, these are just my own work, don't take anything in here as an authoritative source.

* 30-configmaps.yaml
* 40-services.yaml
* 55-statefulsets.yaml

### Deployment Instructions

* `kubectl apply -f zookeeper/`

### Minikube notes
* `kubectl get services` (to see which port forwards to 2181, or 8080, etc.)
* `minikube service zookeeper --url [-n namespace]`

### Configuration
The configmap contains not only the config files that will be used in the zookeeper pods, but also an init script to configure each pod with its id and individual configuration needed to participate as a cluster.

There's no persistent storage yet, because I'm just using this as a lab right now.  I don't have any need to keep any data.

I have tested deleting pods, the deleted pod gets replaced and rejoins the cluster.

### TODO
* Document this better! lol
* Not sure if I want to add persistent storage yet, but have it as an option.
